from waters.Item import Item


class DamageOrb(Item):
    def __init__(self, pos):
        Item.__init__(self, pos, 10, 10)
        self.color = (255, 100, 0)

    def onPickUp(self, picker):
        picker.damageMultiplyer += 0.2
        self.time_passed = self.live_time + 1

