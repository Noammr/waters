import datetime
import random
import time

import clock as clock
import pygame
from physicsObject import *
from staticGameObject import *
from Vector import *
import numpy as np

from waters.CharacterHand import *
from waters.CharacterBody import *


pygame.init()
Black = (0, 0, 0)
White = (255, 255, 255)
Red = (255, 10, 10)
Blue = (10, 10, 255)


surface = pygame.display.set_mode((1280, 700), pygame.SRCALPHA)


hand1 = CharacterHand(Vector2D(500, 200), 15, (255, 150, 10))
ball1 = hand1.physics

hand2 = CharacterHand(Vector2D(500, 200), 15, (125, 255, 10))
ball2 = hand2.physics

hand3 = CharacterHand(Vector2D(500, 200), 15, (10, 10, 255))
ball3 = hand3.physics

hands = [hand1]

heart = CharacterBody(Vector2D(500, 200), 20, Red)

staticTerrain = []
for i in np.arange(0, math.pi * 5, 0.01):
    staticTerrain.append(StaticGameObject(Vector2D(i * 1280 / (math.pi * 5), math.sin(i) * 100 + 300), Black, 10))

# staticTerrain = StaticGameObjectGroup(staticTerrain, ball1.radius)
staticTerrain = StaticGameObjectGroup.makeNoiseGeneratedTerrain(0, 0, 1280, 700, 2, 9, random.randint(0, 1000), 0.2, 10,
                                                                Black, ball1.radius, 10)


def update(dt):
    # ball1.applyGravity()

    heart.physics.applyGravity()

    # if leftClickHeld:
    #     heart.pullToHand(25, 5, hand1, dt)
    # else:
    #     heart.pullToHand(100, 5, hand1, dt)

    # hand1.pullToBody(100, 10, heart)
    for index, hand in enumerate(hands):

        hand.pullToMouse(Vector2D(pygame.mouse.get_pos()[0] + index * 50, pygame.mouse.get_pos()[1]), heart, 100)
        hand.physics.applyForces(dt)

    heart.physics.applyForces(dt)

    for hand in hands:
        counterVel = hand.physics.collideWithStaticGroup(staticTerrain)
        heart.pullToHandCounter(hand, counterVel, power=20)

    heart.physics.collideWithStaticGroup(staticTerrain)


    # pygame.mouse.set_pos((ball1.pos.x, ball1.pos.y))



    heart.physics.updatePos(dt)
    for hand in hands:
        hand.physics.updatePos(dt)

    # heart.render(surface)
    #
    # hand1.render(surface)

    heart.renderBodyWithHands(surface, hands)
    # heart.renderBodyWithHandTeathers(surface, hand1, lastTime)
    staticTerrain.render(surface)

    # print(ball1.activeForces)


clock = pygame.time.Clock()

playing = True
lastTime = time.time()

key_states = {pygame.K_w: False, pygame.K_a: False, pygame.K_s: False, pygame.K_d: False}
leftClickHeld = False

while playing == True:
    surface.fill(White)

    dt = time.time() - lastTime
    lastTime = time.time()
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            playing = False
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_w:
                ball1.vel.y = -100
            elif event.key in key_states:
                key_states[event.key] = True
        elif event.type == pygame.KEYUP:
            if event.key in key_states:
                key_states[event.key] = False
        elif event.type == pygame.MOUSEBUTTONDOWN:
            if event.button == 1:
                leftClickHeld = True
        elif event.type == pygame.MOUSEBUTTONUP:
            if event.button == 1:
                leftClickHeld = False

    if key_states[pygame.K_a]:
        ball1.activeForces.x -= 200

    if key_states[pygame.K_d]:
        ball1.activeForces.x += 200

    update(dt)

    pygame.display.update()

    clock.tick(40)
