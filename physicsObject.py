from Vector import *
from staticGameObject import *


class PhysicsObject:
    def __init__(self, pos, radius, mass=1):
        self.pos = pos
        self.vel = Vector2D(0, 0)
        self.mass = mass
        self.activeForces = Vector2D(0, 0)
        self.radius = radius
        self.touching_surface = False

    def applyForces(self, dt):
        self.vel += dt * self.activeForces / self.mass
        self.activeForces.x = 0
        self.activeForces.y = 0
        # print(dt)

    def applyGravity(self):
        self.activeForces.y += 200 * self.mass

    def updatePos(self, dt):
        self.pos += self.vel * dt

    def collideWithStaticGroup(self, group: StaticGameObjectGroup):
        colliders = group.findValidColliders(self.pos)
        collided = []
        for collider in colliders:
            if self.checkSingleCollisionWithStatic(collider):
                collided.append(collider)
        startVel = self.vel
        counterVel = Vector2D(0, 0)
        for collider in collided:
            counterVel += self.collideWithStaticObj(collider, startVel*1, len(collided))

        if len(collided) > 0:
            self.touching_surface = True
        else:
            self.touching_surface = False
        return counterVel, len(collided) > 0

    def isTouchingTerrain(self, group: StaticGameObjectGroup):
        colliders = group.findValidColliders(self.pos)
        for collider in colliders:
            if self.checkSingleCollisionWithStatic(collider):
                return True
        return False


    def checkSingleCollisionWithStatic(self, obj: StaticGameObject):
        d2 = (obj.pos.x - self.pos.x) ** 2 + (obj.pos.y - self.pos.y) ** 2
        r2 = (obj.radius + self.radius) ** 2
        return r2 > d2

    def collideWithStaticObj(self, obj: StaticGameObject, startVel, n):
        dir: Vector2D = (self.pos - obj.pos).norm()

        # if startVel.magnitude() < 8000:
        # counterVel = min(dir.dot(self.vel), -4/n) * dir
        counterVel = min(dir.dot(self.vel), 0) * dir

        self.vel -= counterVel
        return counterVel
        # print("nownow", startVel.magnitude())
            # obj.color = (255, 0, 0)
        #
        # else:
        #     counterVel = dir.dot(self.vel) * dir
        #     self.vel -= counterVel
        #     print("no", startVel.magnitude())
        #     # obj.color = (0, 255, 0)
        # self.vel += (dir * /n)/self.mass
