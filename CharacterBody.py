import numpy as np

from physicsObject import *
import pygame
from CharacterHand import *
# from waters.CharacterHand import *


class CharacterBody:
    def __init__(self, pos: Vector2D, radius, color):
        self.physics = PhysicsObject(pos, radius)
        self.hp = 100
        self.startingHp = self.hp
        self.color = color
        self.damage = 0
        self.team = 0
        self.invincibleTime = 0
        self.damageMultiplyer = 1
        self.stuckSpawn = 5

    def render(self, surface):
        pygame.draw.circle(surface, self.color, (self.physics.pos.x, self.physics.pos.y), self.physics.radius)

    def pullToHand(self, spring_length, spring_strength, hand, dt):
        if hand.physics.touching_surface:
            bodyToHand: Vector2D = hand.physics.pos - self.physics.pos
            m = bodyToHand.magnitude()
            self.physics.activeForces += bodyToHand.norm() * (m - spring_length) * spring_strength
        #
        # if hand.physics.touching_surface:
        #     dir: Vector2D = (hand.physics.pos - self.physics.pos)
        #     r = dir.magnitude()
        #     dir = dir.norm()
        #     self.physics.vel = ((dir * (r - spring_length) * spring_strength)*10*dt + self.physics.vel)/(1+dt*10)

    def pullToHandCounter(self, hand1, counterVel, collided, power, mouseMovement):

        heartToHand = hand1.physics.pos - self.physics.pos
        r = heartToHand.magnitude()
        heartToHand = heartToHand.norm()
        if r > hand1.body_dist_limit * 1.5:
            hand1.isHolding = False
        if not hand1.isHolding:
            heartPullToHand = counterVel.norm().dot(heartToHand) * heartToHand * power
            self.physics.vel -= heartPullToHand
        else:
            if collided:
                heartPullToHand = mouseMovement.norm().dot(heartToHand) * heartToHand * power
                self.physics.vel -= heartPullToHand

    def renderBodyWithHandTeathers(self, surface, hand, time):
        time = time*4
        minX = min(self.physics.pos.x, hand.physics.pos.x)
        maxX = max(self.physics.pos.x, hand.physics.pos.x)
        if maxX == minX:
            return
        scales = np.array([-np.sin(time+6.3)*20, np.sin(time+2.5)*30, -np.sin(time+0.6)*10, np.sin(time+0.2)*20, np.cos(time)*30])*20/max((self.physics.pos-hand.physics.pos).magnitude(), 40)
        for t in np.arange(0, 1, 0.01):
            for i, scale in enumerate(scales):
                angle = t * np.pi * i
                l = np.sin(angle) * scale
                theta = np.arctan2(hand.physics.pos.y - self.physics.pos.y,hand.physics.pos.x - self.physics.pos.x) + np.pi/2
                pos = Vector2D(self.physics.pos.x*t + hand.physics.pos.x*(1-t), self.physics.pos.y*t + hand.physics.pos.y*(1-t))
                pos += Vector2D(np.cos(theta), np.sin(theta)) * l
                pygame.draw.rect(surface, (0, 0, 0), (pos.x, pos.y, 4, 4))


    def renderBodyWithHand(self, surface, hand):
        fakeRadius = 1/(self.physics.radius**2)
        fakeRadiusHand = 1/(hand.physics.radius**2)

        fakeRadiushalf = 1 / ((self.physics.radius/3) ** 2)
        fakeRadiusHandhalf = 1 / ((hand.physics.radius/3) ** 2)

        fakeRadiusOut = 1 / ((self.physics.radius*1.5) ** 2)
        fakeRadiusHandOut = 1 / ((hand.physics.radius*1.5) ** 2)


        for x in range(int(min(hand.physics.pos.x, self.physics.pos.x) - max(self.physics.radius, hand.physics.radius)), int(max(hand.physics.pos.x, self.physics.pos.x) + max(self.physics.radius, hand.physics.radius)), 2):
            for y in range(int(min(hand.physics.pos.y, self.physics.pos.y) - max(self.physics.radius, hand.physics.radius)), int(max(hand.physics.pos.y, self.physics.pos.y) + max(self.physics.radius, hand.physics.radius)), 2):
                p1 = CharacterBody.calcFakeDist(x, y, self.physics.pos.x, self.physics.pos.y)
                p2 = CharacterBody.calcFakeDist(x, y, hand.physics.pos.x, hand.physics.pos.y)
                p1div = p1/(p1+p2)
                p2div = 1 - p1div


                if p1 + p2 >= fakeRadius*p1div + fakeRadiusHand*p2div: #0.001
                    if ((p1 > p2 or random.random() > p2 / (4 * p1)) and not random.random() > p1 / (4 * p2)):
                        s = int(p1 * 500)

                        s = (1 / p1 ** 0.5) / self.physics.radius
                        pygame.draw.rect(surface, (CharacterBody.colorCap(self.color[0] * s),
                                                   CharacterBody.colorCap(self.color[1] * s),
                                                   CharacterBody.colorCap(self.color[2] * s)),
                         (x-1, y-1, 2, 2))
                    else:
                        s = int(p2 * 500)
                        s = (1/p2**0.5) / hand.physics.radius

                        pygame.draw.rect(surface, (CharacterBody.colorCap(hand.color[0] * s),
                                                   CharacterBody.colorCap(hand.color[1] * s),
                                                   CharacterBody.colorCap(hand.color[2] * s)),
                        (x-1, y-1, 2, 2))


    def renderOnlyBody(self, surface, offset):
        for x in range(int(self.physics.pos.x) - self.physics.radius,
                       int(self.physics.pos.x) + self.physics.radius, 2):
            for y in range(int(self.physics.pos.y) - self.physics.radius,
                           int(self.physics.pos.y) + self.physics.radius, 2):
                p1 = CharacterBody.calcFakeDist(x, y, self.physics.pos.x, self.physics.pos.y)
                fakeRadius1 = 1 / (self.physics.radius ** 2)
                if p1 >= fakeRadius1:
                    s = (1 / p1 ** 0.5) / self.physics.radius
                    pygame.draw.rect(surface, (CharacterBody.colorCap(self.color[0] * s),
                                               CharacterBody.colorCap(self.color[1] * s),
                                               CharacterBody.colorCap(self.color[2] * s)),
                                     (x - 1 - offset.x, y - 1 - offset.y, 2, 2))

    def renderBodyWithHands(self, surface, hands, offset):
        if len(hands) < 1:
            self.renderOnlyBody(surface, offset)
            return
        maxHandsX = max([hand.physics.pos.x for hand in hands])
        maxHandsY = max([hand.physics.pos.y for hand in hands])
        minHandsX = min([hand.physics.pos.x for hand in hands])
        minHandsY = min([hand.physics.pos.y for hand in hands])
        maxHandsR = max([hand.physics.radius for hand in hands])

        for x in range(int(min(minHandsX, self.physics.pos.x) - max(self.physics.radius, maxHandsR)), int(max(maxHandsX, self.physics.pos.x) + max(self.physics.radius, maxHandsR)), 2):
            for y in range(int(min(minHandsY, self.physics.pos.y) - max(self.physics.radius, maxHandsR)), int(max(maxHandsY, self.physics.pos.y) + max(self.physics.radius, maxHandsR)), 2):
                pBody = CharacterBody.calcFakeDist(x, y, self.physics.pos.x, self.physics.pos.y)
                ps = [pBody]
                ps.extend([CharacterBody.calcFakeDist(x, y, hand.physics.pos.x, hand.physics.pos.y) for hand in hands])
                ob1, ob2, p1, p2 = self.highestBodies(hands, ps)

                p1div = p1/(p1+p2)
                p2div = 1 - p1div

                fakeRadius1 = 1 / (ob1.physics.radius ** 2)
                fakeRadius2 = 1 / (ob2.physics.radius ** 2)

                if p1 + p2 >= fakeRadius1*p1div + fakeRadius2*p2div:  # 0.001
                    if (p1 > p2 or random.random() > p2 / (2 * p1)) and not random.random() > p1 / (2 * p2):

                        s = (1 / p1 ** 0.5) / ob1.physics.radius
                        pygame.draw.rect(surface, (CharacterBody.colorCap(ob1.color[0] * s),
                                                   CharacterBody.colorCap(ob1.color[1] * s),
                                                   CharacterBody.colorCap(ob1.color[2] * s)),
                         (x-1 - offset.x, y-1 - offset.y, 2, 2))
                    else:

                        s = (1/p2**0.5) / ob2.physics.radius

                        pygame.draw.rect(surface, (CharacterBody.colorCap(ob2.color[0] * s),
                                                   CharacterBody.colorCap(ob2.color[1] * s),
                                                   CharacterBody.colorCap(ob2.color[2] * s)),
                        (x-1 - offset.x, y-1 - offset.y, 2, 2))


        pygame.draw.rect(surface, (255, 0, 0), (self.physics.pos.x - offset.x - self.physics.radius, self.physics.pos.y - offset.y - 1.5*self.physics.radius, self.physics.radius*2, 5))
        pygame.draw.rect(surface, (0, 255, 0), (self.physics.pos.x - offset.x - self.physics.radius, self.physics.pos.y - offset.y - 1.5*self.physics.radius, self.physics.radius*2*self.hp/self.startingHp, 5))


    import numpy as np

    def renderBodyWithHandsopt(self, surface, hands):
        # Precompute values that remain constant
        maxHandsX = max(hand.physics.pos.x for hand in hands)
        maxHandsY = max(hand.physics.pos.y for hand in hands)
        minHandsX = min(hand.physics.pos.x for hand in hands)
        minHandsY = min(hand.physics.pos.y for hand in hands)
        maxHandsR = max(hand.physics.radius for hand in hands)
        fakeRadius = 1 / (self.physics.radius ** 2)
        fakeRadiusHands = [1 / (hand.physics.radius ** 2) for hand in hands]
        fakeRads = [fakeRadius]
        fakeRads.extend(fakeRadiusHands)
        fakeRads = np.array(fakeRads)

        for x in range(int(min(minHandsX, self.physics.pos.x) - max(self.physics.radius, maxHandsR)),
                       int(max(maxHandsX, self.physics.pos.x) + max(self.physics.radius, maxHandsR)), 4):
            for y in range(int(min(minHandsY, self.physics.pos.y) - max(self.physics.radius, maxHandsR)),
                           int(max(maxHandsY, self.physics.pos.y) + max(self.physics.radius, maxHandsR)), 4):
                # Calculate fake distances and probabilities
                pBody = CharacterBody.calcFakeDist(x, y, self.physics.pos.x, self.physics.pos.y)
                ps = [pBody]
                ps.extend(CharacterBody.calcFakeDist(x, y, hand.physics.pos.x, hand.physics.pos.y) for hand in hands)

                objs = [self]
                objs.extend(hands)
                divs = np.array(ps) / np.sum(ps)

                threshold = np.sum(fakeRads * np.array(divs))

                if np.sum(ps) >= threshold:
                    # Calculate color using weighted sum
                    col = np.sum(np.array([[obj.color[0], obj.color[1], obj.color[2]] for obj in objs]) * divs[:, None],
                                 axis=0)
                    col = tuple(CharacterBody.colorCap(c) for c in col)
                    pygame.draw.rect(surface, col, (x - 2, y - 2, 4, 4))

    def highestBodies(self, hands, ps):
        maxP = 0
        maxI = 0
        for index, p in enumerate(ps):
            if p > maxP:
                maxP = p
                maxI = index
        p1 = maxP
        ob1 = None
        if maxI == 0:
            ob1 = self
        else:
            ob1 = hands[maxI - 1]
        ps[maxI] = -100000
        maxP = 0
        maxI = 0
        for index, p in enumerate(ps):
            if p > maxP:
                maxP = p
                maxI = index
        p2 = maxP
        ob2 = None
        if maxI == 0:
            ob2 = self
        else:
            ob2 = hands[maxI - 1]

        return ob1, ob2, p1, p2

    def take_damage(self, damage):
        if self.invincibleTime <= 0:
            self.hp -= damage
            print("heart took damage")
            self.invincibleTime = 0.5

    @staticmethod
    def colorCap(v):
        if v > 255:
            return 255
        if v < 0:
            return 0
        return int(v)

    @staticmethod
    def calcFakeDist(x, y, a, b):
        d = ((x - a) * (x - a) + (y - b) * (y - b))
        if (d == 0):
            return 1000
        return 1 / d


