import random

import numpy as np

from physicsObject import *
from Vector import *
import pygame
import noise


class StaticGameObject:
    def __init__(self, pos, color, radius):
        self.radius = radius
        self.pos = pos
        self.color = color

    def renderCircle(self, surface, offset):
        pygame.draw.circle(surface, self.color, (self.pos.x - offset.x, self.pos.y - offset.y), self.radius)

    def renderSquare(self, surface):
        pygame.draw.rect(surface, self.color, (self.pos.x, self.pos.y, self.radius, self.radius))

    def pr(self):
        print(self.pos.x, self.pos.y)


class StaticGameObjectGroup:
    def __init__(self, objects, interactor_radius):
        r = interactor_radius
        minX = objects[0].pos.x
        maxX = objects[0].pos.x
        minY = objects[0].pos.y
        maxY = objects[0].pos.y
        for obj in objects:
            if obj.radius > r:
                r = obj.radius
            if obj.pos.x < minX:
                minX = obj.pos.x
            if obj.pos.x > maxX:
                maxX = obj.pos.x
            if obj.pos.y < minY:
                minY = obj.pos.y
            if obj.pos.y > maxY:
                maxY = obj.pos.y
        self.maxRadius = r
        self.startX = minX
        self.startY = minY
        self.endX = maxX
        self.endY = maxY

        self.objectsXopt = []

        n = math.floor((maxX - minX) / self.maxRadius) + 1
        m = math.floor((maxY - minY) / self.maxRadius) + 1




        for x in range(n):
            self.objectsXopt.append([])
            for y in range(m):
                self.objectsXopt[-1].append([])

        for obj in objects:
            i = math.floor((obj.pos.x - self.startX) / self.maxRadius)
            j = math.floor((obj.pos.y - self.startY) / self.maxRadius)
            self.objectsXopt[i][j].append(obj)

    @staticmethod
    def makeNoiseGeneratedTerrain(x_left, y_top, width, height, scaleX, scaleY, seed, cutoff, radius, color, interactor_radius, skip_range=10):
        # Generate empty 2D array to store Perlin noise values
        noise_map = np.zeros((height, width))

        objects = []

        # Generate Perlin noise values for each pixel in the array
        for y in np.arange(0, height, skip_range):
            for x in np.arange(0, width, skip_range):
                # Calculate the coordinates in the noise space
                nx = x / 1280
                ny = y / 720

                # Generate Perlin noise value at current coordinates
                # print(noise.pnoise2(nx * scaleX + seed, ny * scaleY + seed))
                power = noise.pnoise2(nx * scaleX + seed, ny * scaleY + seed)
                if power >= cutoff:
                    objects.append(StaticGameObject(Vector2D((noise.pnoise2(x*y/421, x*y/21)*100.1241)%5.4 + x + x_left, (noise.pnoise2(x*y/212, x*y/512)*120.3425)%5.4 + y + y_top), StaticGameObjectGroup.grayScale(power + 0.5), radius))
                    # print("color", power, int(power * 255))

        for x in range(x_left, x_left + width):
            objects.append(StaticGameObject(
                Vector2D(x, y_top),
                (100, 100, 100), radius))
            objects.append(StaticGameObject(
                Vector2D(x, y_top + height),
                (100, 100, 100), radius))

        for y in range(y_top, y_top + height):
            objects.append(StaticGameObject(
                Vector2D(x_left, y),
                (100, 100, 100), radius))
            objects.append(StaticGameObject(
                Vector2D(x_left + width, y),
                (100, 100, 100), radius))

        return StaticGameObjectGroup(objects, interactor_radius)

    @staticmethod
    def grayScale(power):
        c = int(power * 255)
        if(c < 0):
            c = 0
        if(c > 255):
            c = 255
        return (c, c, c)

    def findValidColliders(self, colliderPos):
        valid_obs = []
        i = math.floor((colliderPos.x - self.startX) / self.maxRadius)
        j = math.floor((colliderPos.y - self.startY) / self.maxRadius)
        for deltaI in range(-2, 3):
            if 0 <= i + deltaI < len(self.objectsXopt):
                for deltaJ in range(-2, 3):
                    if 0 <= j + deltaJ < len(self.objectsXopt[i + deltaI]):
                        valid_obs.extend(self.objectsXopt[i + deltaI][j + deltaJ])
                        # for k in self.objectsXopt[i + deltaI][j + deltaJ]:
                            # k.color = (255, 255, 0)

        return valid_obs

    def render(self, surface, offset):
        minXindex = max(math.floor((offset.x - self.startX) / self.maxRadius), 0)
        maxXindex = min(math.floor((offset.x + 1280 - self.startX) / self.maxRadius) + 1, len(self.objectsXopt) - 1)
        minYindex = max(math.floor((offset.y - self.startY) / self.maxRadius), 0)
        maxYindex = min(math.floor((offset.y + 700 - self.startY) / self.maxRadius) + 1, len(self.objectsXopt[0]) - 1)
        for i in self.objectsXopt[minXindex: maxXindex]:
            for j in i[minYindex: maxYindex]:
                for k in j:
                    k.renderCircle(surface, offset)



# obs = []
# for i in range(100):
#     obs.append(StaticGameObject(Vector2D(random.randint(0, 45), random.randint(9, 51)), (0, 0, 0), 10))
#
# obs = StaticGameObjectGroup(obs, 15)
# for arr in obs.objectsXopt:
#     print("|")
#     for ob in arr:
#         print("_")
#         for o in ob:
#             print(o.pos.x, o.pos.y)
# obs.findValidColliders(Vector2D(10, 15))
