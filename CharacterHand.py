from physicsObject import *
import pygame
from CharacterBody import *


class CharacterHand:
    def __init__(self, pos: Vector2D, radius, color):
        self.physics = PhysicsObject(pos, radius)
        self.hp = 100
        self.color = color
        self.heldColor = (0, 0, 255)
        self.originalColor = color
        self.lastM = None
        self.lastTarget = None
        self.shooting_cooldown = 0
        self.damage = 0
        self.team = 0
        self.isHolding = False
        self.body_dist_limit = 0
        self.pull_force = Vector2D(0, 0)
        self.normalRadius = radius
        self.heldRadius = self.normalRadius * 2
        self.stuckSpawn = 5

    def render(self, surface):
        pygame.draw.circle(surface, self.color, (self.physics.pos.x, self.physics.pos.y), self.physics.radius)

    def pullToBody(self, spring_length, spring_strength, body):
        dir:Vector2D = (body.physics.pos - self.physics.pos)
        r = dir.magnitude()
        dir = dir.norm()
        self.physics.activeForces += dir * (r - spring_length) * spring_strength



    def pullToMouse(self, mouse_vector: Vector2D, body, body_dist_limit: float):
        mouse_abs = mouse_vector + body.physics.pos - Vector2D(400, 400)
        self.body_dist_limit = body_dist_limit
        bodyToHand = self.physics.pos - body.physics.pos
        handToMouse = mouse_abs - self.physics.pos
        bodyToMouse: Vector2D = bodyToHand + handToMouse

        m = bodyToMouse.magnitude()
        if m > body_dist_limit:
            bodyToMouse = bodyToMouse.norm() * body_dist_limit

        handToSetPoint = bodyToMouse - bodyToHand
        self.physics.vel = handToSetPoint*4# + dir * d * 0.4
        self.pull_force = self.physics.vel.copy()

        # direction: Vector2D = (mouse_vector - self.physics.pos)
        # m = direction.magnitude()
        #
        # if m > 100:
        #     direction = direction.norm() * 100
        #
        # d = 0
        # if self.lastM is not None:
        #     d = m - self.lastM
        #     # if -1 < d < 1:
        #     #     d = d/abs(d)
        #
        # self.physics.vel = direction*4# + dir * d * 0.4
        # self.lastM = m

    def update_cooldown(self, dt):
        self.shooting_cooldown -= dt

    def take_damage(self, d):
        pass