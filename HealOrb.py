from waters.Item import Item


class HealOrb(Item):
    def __init__(self, pos):
        Item.__init__(self, pos, 10, 10)
        self.color = (0, 255, 0)

    def onPickUp(self, picker):
        picker.hp += 20
        self.time_passed = self.live_time + 1

