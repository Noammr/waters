import pygame.draw

from physicsObject import  *

class Lazer:
    def __init__(self, pos, radius, damage, explosion_radius, detonation_time):
        self.physics = PhysicsObject(pos, radius)
        self.damage = damage
        self.explosion_radius = explosion_radius
        self.time_alive = 0
        self.detonation_time = detonation_time
        self.explode = False

    def updatePhysics(self, dt, terrain, enemies):
        self.time_alive += dt
        if self.time_alive >= self.detonation_time:
            self.explode = True


        resistance, collided = self.physics.collideWithStaticGroup(terrain)
        if resistance.magnitude() > 0.01:
            self.explode = True
        self.physics.updatePos(dt)

        for enemy in enemies:
            if (enemy.physics.pos - self.physics.pos).magnitude() <= self.physics.radius + enemy.physics.radius:
                enemy.take_damage(self.damage)
                self.explode = True


    def render(self, surface, offset):
        for i in range(4):
            pygame.draw.circle(surface, (0, 255, 0),
                           (self.physics.pos.x - self.physics.vel.x*i*0.005 - offset.x, self.physics.pos.y - self.physics.vel.y*i*0.005 - offset.y), self.physics.radius)

    def on_death(self, targets):
        pass

    @staticmethod
    def colorCap(v):
        if v > 255:
            return 255
        if v < 0:
            return 0
        return int(v)





