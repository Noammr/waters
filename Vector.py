import math

class Vector2D:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __repr__(self):
        return f"Vector2D({self.x}, {self.y})"

    def __add__(self, other):
        return Vector2D(self.x + other.x, self.y + other.y)

    def __sub__(self, other):
        return Vector2D(self.x - other.x, self.y - other.y)

    def __mul__(self, scalar):
        return Vector2D(self.x * scalar, self.y * scalar)

    def __rmul__(self, scalar):
        return self * scalar

    def __truediv__(self, scalar):
        if scalar == 0:
            raise ValueError("Cannot divide by zero")
        return Vector2D(self.x / scalar, self.y / scalar)

    def dot(self, other):
        return self.x * other.x + self.y * other.y

    def magnitude(self):
        return math.sqrt(self.x ** 2 + self.y ** 2)

    def norm(self):
        m = self.magnitude()
        if m == 0:
            m = 1
        return Vector2D(self.x/m, self.y/m)

    def copy(self):
        return Vector2D(self.x, self.y)

    def rotate(self, rads):
        rotator = math.cos(rads) + math.sin(rads)*1j
        this = (self.x + self.y*1j) * rotator
        self.x = this.real
        self.y = this.imag
        return self
