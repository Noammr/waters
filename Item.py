import math

import pygame


class Item:
    def __init__(self, pos, radius, live_time):
        self.time_passed = 0
        self.pos = pos
        self.radius = radius
        self.live_time = live_time

    def update(self, dt):
        self.time_passed += dt
        if self.time_passed > self.live_time:
            return True

    def render(self, surface, offset):
        pygame.draw.circle(surface, self.color, (self.pos.x - offset.x, self.pos.y - offset.y), self.radius * (self.live_time - self.time_passed)//self.live_time)