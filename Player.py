import pickle

from Vector import *


class Player:

    def __init__(self, character, i):
        self.character = character
        self.mousePos = Vector2D(0, 0)
        self.events = []
        self.name = "player"+str(i)
        self.held = {"mouse 1": False}
        self.updated = False
        self.lastMousePos = Vector2D(0, 0)

    def updatePlayerData(self, message):
        segments = message.split(b";;;")
        for segment in segments:
            # print(segment)
            args = segment.split(b":::")
            param_name = args[0]
            value = args[1]
            # setattr(self, param_name.decode(), value)
            if param_name == b"mousePos":
                self.setMousePos(value)
            elif param_name == b"name":
                self.setName(value)
            elif param_name == b"events":
                self.setEvents(value)


    def setMousePos(self, mouse_str):
        self.lastMousePos = self.mousePos.copy()
        self.mousePos.x = float(mouse_str.split(b",")[0])
        self.mousePos.y = float(mouse_str.split(b",")[1])

    def getMouseMovement(self):
        return self.mousePos - self.lastMousePos

    def setEvents(self, events_str):
        self.events = pickle.loads(events_str)
        if len(self.events) > 0:
            print("eventsssssss", self.events)
        self.updated = True

    def setName(self, name_str):
        self.name = name_str







        """ protocol
        shape of a message
        
        param0::: data;;; param1::: data;;;
        
        """