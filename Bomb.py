import pygame.draw

from physicsObject import  *

class Bomb:
    def __init__(self, pos, radius, damage, explosion_radius, detonation_time):
        self.physics = PhysicsObject(pos, radius)
        self.damage = damage
        self.explosion_radius = explosion_radius
        self.time_alive = 0
        self.detonation_time = detonation_time
        self.explode = False

    def updatePhysics(self, dt, terrain, enemies):
        self.time_alive += dt
        if self.time_alive >= self.detonation_time:
            self.explode = True
        self.physics.applyGravity()

        self.physics.applyForces(dt)

        self.physics.collideWithStaticGroup(terrain)

        self.physics.updatePos(dt)

    def on_death(self, targets):
        for target in targets:
            dir = target.physics.pos - self.physics.pos
            if dir.magnitude() <= self.explosion_radius:
                target.take_damage(self.damage)
                target.physics.activeForces += dir.norm()*20000

    def render(self, surface, offset):
        if self.detonation_time - self.time_alive > 0.1:
            pygame.draw.circle(surface, (Bomb.colorCap(255 * self.time_alive / self.detonation_time), 0, 0),
                           (self.physics.pos.x - offset.x, self.physics.pos.y - offset.y), self.physics.radius)
        else:
            pygame.draw.circle(surface, (200, 200, 0),
                               (self.physics.pos.x - offset.x, self.physics.pos.y - offset.y), self.explosion_radius)


    @staticmethod
    def colorCap(v):
        if v > 255:
            return 255
        if v < 0:
            return 0
        return int(v)





