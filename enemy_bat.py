from physicsObject import *

class EnemyBat:
    def __init__(self, pos, size, hp, damage, level):
        self.physics = PhysicsObject(pos, size, 0.2)
        self.hp = hp
        self.damage = damage
        self.timeAlive = 0
        self.startingHp = self.hp
        self.framesStuck = 0
        self.team = 1
        self.level = level
        self.stuckDead = False

    def updatePhysics(self, dt, terrain, target):
        self.timeAlive += dt
        direction = target.physics.pos - self.physics.pos
        direction.rotate(math.sin(self.timeAlive*10))
        self.physics.activeForces += direction*1
        self.physics.applyForces(dt)


        recoil, collided = self.physics.collideWithStaticGroup(terrain)
        self.physics.vel -= 0.2*recoil
        if self.physics.vel.magnitude() > 150:
            self.physics.vel = self.physics.vel.norm() * 150
        self.physics.updatePos(dt)

        if self.physics.pos.x > terrain.endX or self.physics.pos.x < terrain.startX or self.physics.pos.y > terrain.endY or self.physics.pos.y < terrain.startY:
            self.hp = 0
            self.stuckDead = True

        if self.physics.vel.magnitude() < 10:
            self.framesStuck += 1
            if self.framesStuck > 4:
                self.hp = 0
                self.stuckDead = True
        else:
            self.framesStuck = 0




    def render(self, surface, offset, target):
        direction = (target.physics.pos - self.physics.pos).norm() * self.physics.radius
        pygame.draw.circle(surface, (0, 0, 0), (self.physics.pos.x - offset.x, self.physics.pos.y - offset.y), self.physics.radius)

        for i in range(0, self.level):
            pygame.draw.circle(surface, (255, 0, 0), (self.physics.pos.x + direction.x - offset.x, self.physics.pos.y + direction.y  - offset.y), self.physics.radius/4)
            direction *= 0.75


        pygame.draw.rect(surface, (255, 0, 0), (self.physics.pos.x - offset.x - self.physics.radius, self.physics.pos.y - offset.y - 1.5*self.physics.radius, self.physics.radius*2, 5))
        pygame.draw.rect(surface, (0, 255, 0), (self.physics.pos.x - offset.x - self.physics.radius, self.physics.pos.y - offset.y - 1.5*self.physics.radius, self.physics.radius*2*self.hp/self.startingHp, 5))

    @staticmethod
    def checkLiveCollisions(colliders):
        for i in range(len(colliders)):
            for j in range(i+1, len(colliders)):
                diff = colliders[i].physics.pos - colliders[j].physics.pos
                m = diff.magnitude()
                if m < colliders[i].physics.radius + colliders[j].physics.radius and not (colliders[i].team + colliders[j].team == 0):
                    dir = diff.norm()
                    colliders[i].physics.activeForces += dir*1000
                    colliders[j].physics.activeForces -= dir*1000
                    if colliders[i].team != colliders[j].team:
                        colliders[i].take_damage(colliders[j].damage)
                        colliders[j].take_damage(colliders[i].damage)


    def take_damage(self, damage):
        self.hp -= damage
