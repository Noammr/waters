import datetime
import random
import time

import clock as clock
import pygame
from physicsObject import *
from Vector import *

pygame.init()

surface = pygame.display.set_mode((1280, 700), pygame.SRCALPHA)

ball1 = physicsObject(Vector2D(500, 100))
ball1.vel.x = -110

ball2 = physicsObject(Vector2D(580, 120))
ball2.vel.x = -110

lastTime = time.time()
print(lastTime)
dt = 0

WHITE = (255, 255, 255)
BLACK = (0, 0, 0)

BREEN = (50, 50, 150)
FOAM = (200, 200, 200)




def update(dt):
    p = 0.8
    if ball2.pos.y > 500 :
        if ball2.vel.y > 0:
            ball2.vel.y *= -p
    if ball1.pos.y > 500:
        if ball1.vel.y > 0:
            ball1.vel.y *= -p

    if ball2.pos.x > 600 :
        if ball2.vel.x > 0:
            ball2.vel.x *= -1
    if ball1.pos.x > 600:
        if ball1.vel.x > 0:
            ball1.vel.x *= -1

    if ball2.pos.x < 500 :
        if ball2.vel.x < 0:
            ball2.vel.x *= -1
    if ball1.pos.x < 500:
        if ball1.vel.x < 0:
            ball1.vel.x *= -1
    # collide(ball1, ball2)

    ball1.applyGravity()
    ball1.applyForces(dt)
    ball2.applyGravity()
    ball2.applyForces(dt)
    ball2.updatePos(dt)
    ball1.updatePos(dt)

    print(dt, random.random())

    for x in range(300, 700, 4):
        for y in range(100, 600, 4):
            p1 =  calcFakeDist(x, y, ball1.pos.x, ball1.pos.y)
            p2 = calcFakeDist(x, y, ball2.pos.x, ball2.pos.y)

            if p1 + p2 >= 0.001:
                if((p1 > p2 or random.random() > p2/(4*p1)) and not random.random() > p1/(4 * p2)):
                    r = int(200 *p1 * 500)
                    print(p1)
                    if(r > 255):
                        r = 255
                    # pygame.draw.rect(surface, (r, r, r, 100), (x, y, 4, 4))
                else:
                    s = int(p2 * 500)
                    pygame.draw.rect(surface, (colorCap(200 * s), colorCap(100 * s), colorCap(20 * s)), (x, y, 4, 4))


                # print(x, y)

    # print("made", made)

def colorCap(v):
    if(v > 255):
        return 255
    return int(v)

def calcFakeDist(x, y, a, b):
    d = ((x - a)*(x - a) + (y - b)*(y - b))
    if(d == 0):
        return 1000
    return 1/d

def collide(b1, b2):
    dir = (b1.pos - b2.pos)
    mag = dir.magnitude()
    if(mag < 80):
        dir = dir/mag
        b1.activeForces += dir * (80 - mag)
        b2.activeForces -= dir * (80 - mag)






clock = pygame.time.Clock()

playing = True

while playing == True:
    surface.fill(WHITE)

    dt = time.time() - lastTime
    lastTime = time.time()
    update(dt)
    for event in pygame.event.get():
       if event.type == pygame.QUIT:
           playing = False

    pygame.display.update()

    clock.tick(40)
