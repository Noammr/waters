import ipaddress
import random
import socket
import threading

import pygame
from scapy.layers.inet import IP, UDP
from scapy.packet import Raw
import scapy

from Button import Button
from WorldData import WorldData
from Client import Client


class Menu:
    def __init__(self, useLocalGiven=None):
        pygame.init()

        self.surface = pygame.display.set_mode((1280, 700), pygame.SRCALPHA)
        self.clock = pygame.time.Clock()
        self.WHITE = (255, 255, 255)
        self.BLACK = (0, 0, 0)
        if useLocalGiven == None:
            localM = input("Do you want to use 127.0.0.x Y/N")
            while localM != "Y" and localM != "N":
                localM = input("Do you want to use 127.0.0.x Y/N")
            self.useLocal = localM == "Y"


        else:
            self.useLocal = useLocalGiven
        self.local_ip = socket.gethostbyname(socket.gethostname())
        if self.useLocal:
            self.local_ip = "127.0.0." + str(random.randint(1, 200))

        self.hosting = False
        self.joined = False
        self.playing = False
        self.IPJoined = '0.0.0.0'

        self.hostButton = Button(x=300, y=250, width=200, height=100, text="host with your address: " + self.local_ip,
                                 font=None, font_size=36, text_color=(255, 255, 255),
                                 button_color=(0, 128, 0), action=None)

        self.startButton = Button(x=300, y=250, width=200, height=100, text="start game",
                                  font=None, font_size=36, text_color=(255, 255, 255),
                                  button_color=(0, 128, 0), action=None)

        self.instructions = [Button(x=1000, y=250, width=200, height=50, text="e: lazer",
                                    font=None, font_size=20, text_color=(255, 255, 255),
                                    button_color=(128, 128, 0), action=None),
                             Button(x=1000, y=300, width=200, height=50, text="q:bomb",
                                    font=None, font_size=20, text_color=(255, 255, 255),
                                    button_color=(0, 128, 0), action=None),
                             Button(x=1000, y=350, width=200, height=50, text="move by pushing off",
                                    font=None, font_size=20, text_color=(255, 255, 255),
                                    button_color=(0, 128, 128), action=None),
                             Button(x=1000, y=400, width=200, height=50, text="walls, hold mouse grab",
                                    font=None, font_size=20, text_color=(255, 255, 255),
                                    button_color=(100, 0, 128), action=None),
                             Button(x=1000, y=450, width=200, height=50, text="orbs: green-health increase",
                                    font=None, font_size=20, text_color=(255, 255, 255),
                                    button_color=(128, 0, 128), action=None),
                             Button(x=1000, y=500, width=200, height=50, text="orbs: orange-damage increase",
                                    font=None, font_size=20, text_color=(255, 255, 255),
                                    button_color=(128, 0, 100), action=None)
                             ]

        self.joinButtons = []
        self.joinIPs = []

        self.playersButtons = []
        self.playersIP = []

        self.network = ipaddress.ip_network(self.local_ip + '/24', strict=False)
        self.broadcast_ip = str(self.network.broadcast_address)

        self.dst_port = 12345
        self.hoster_message = "join me"

        self.udp_packet = IP(dst=self.broadcast_ip, src=self.local_ip) / UDP(dport=self.dst_port,
                                                                             sport=self.dst_port) / Raw(
            load=self.hoster_message)

        self.font = pygame.font.Font(None, 36)

    def listen_for_host(self):
        packets = None
        if self.useLocal:
            packets = scapy.sendrecv.sniff(count=10, lfilter=self.host_filter, timeout=0.2, iface='lo0')
        else:
            packets = scapy.sendrecv.sniff(count=10, lfilter=self.host_filter, timeout=0.2)
        return packets

    def host_filter(self, packet):
        if IP in packet and UDP in packet and packet[UDP].dport == self.dst_port and packet[
            Raw].load == self.hoster_message.encode():
            return True
        return False

    def listen_for_players(self):
        packets = None
        if self.useLocal:
            packets = scapy.sendrecv.sniff(count=10, lfilter=self.player_filter, timeout=0.2, iface='lo0')
        else:
            packets = scapy.sendrecv.sniff(count=10, lfilter=self.player_filter, timeout=0.2)
        return packets

    def player_filter(self, packet):
        if IP in packet and packet[IP].dst == self.local_ip and UDP in packet and packet[UDP].dport == self.dst_port and \
                packet[Raw].load == "I joined you".encode():
            return True
        return False

    def start_game_filter(self, packet):
        if IP in packet and packet[IP].dst == self.local_ip and UDP in packet and packet[UDP].dport == self.dst_port and \
                packet[Raw].load == "starting".encode():
            return True
        return False

    def run(self):
        while not self.playing:
            self.surface.fill(self.BLACK)

            for inst in self.instructions:
                inst.draw(self.surface)

            if not self.hosting and not self.joined:
                self.hostButton.draw(self.surface)
                for joinButton in self.joinButtons:
                    joinButton.draw(self.surface)

                for event in pygame.event.get():
                    if event.type == pygame.QUIT:
                        self.playing = False
                    if self.hostButton.is_clicked(event):
                        print("Host button clicked")
                        self.hosting = True
                        self.hostButton.text = "now hosting on: " + self.local_ip
                    for joinButton in self.joinButtons:
                        if joinButton.is_clicked(event):
                            print("Join button clicked")
                            self.joined = True
                            self.IPJoined = joinButton.text.split(":")[-1]

            if self.hosting:
                scapy.sendrecv.send(self.udp_packet)
                self.startButton.draw(self.surface)

                for button in self.playersButtons:
                    button.draw(self.surface)

                packets = self.listen_for_players()
                for packet in packets:
                    if packet[IP].src not in self.playersIP:
                        self.playersIP.append(packet[IP].src)
                        self.playersButtons.append(
                            Button(x=600, y=len(self.playersButtons) * 200, width=200, height=100,
                                   text='player: ' + packet[IP].src,
                                   font=None, font_size=36, text_color=(255, 255, 255),
                                   button_color=(0, 128, 0), action=None)
                        )

                for event in pygame.event.get():
                    if event.type == pygame.QUIT:
                        self.playing = False

                    if self.startButton.is_clicked(event):
                        print("start game")
                        self.playing = True

            elif not self.joined:
                packets = self.listen_for_host()
                for packet in packets:
                    if packet[IP].src not in self.joinIPs:
                        self.joinIPs.append(packet[IP].src)
                        self.joinButtons.append(
                            Button(x=600, y=len(self.joinButtons) * 200, width=200, height=100,
                                   text='join:' + packet[IP].src,
                                   font=None, font_size=36, text_color=(255, 255, 255),
                                   button_color=(0, 128, 0), action=None)
                        )

            if self.joined:
                joined_packet = IP(dst=self.IPJoined, src=self.local_ip) / UDP(dport=self.dst_port,
                                                                               sport=self.dst_port) / Raw(
                    load="I joined you")
                scapy.sendrecv.send(joined_packet)
                text = self.font.render("waiting for " + self.IPJoined + " to start game", True, (255, 255, 255))
                text_rect = text.get_rect(center=(500, 200))
                self.surface.blit(text, text_rect)

                packets = None
                if self.useLocal:
                    packets = scapy.sendrecv.sniff(count=1, lfilter=self.start_game_filter, timeout=0.5, iface='lo0')
                else:
                    packets = scapy.sendrecv.sniff(count=1, lfilter=self.start_game_filter, timeout=0.5)

                if len(packets) > 0:
                    self.playing = True

            pygame.display.flip()
            self.clock.tick(40)

        if self.hosting:
            worldHost = WorldData(self.local_ip)
            worldThread = threading.Thread(target=worldHost.run_game)
            worldThread.start()
            for address in self.playersIP:
                start_game_packet = IP(dst=address, src=self.local_ip) / UDP(dport=self.dst_port,
                                                                             sport=self.dst_port) / Raw(load="starting")
                scapy.sendrecv.send(start_game_packet)
            client = Client(self.local_ip)
            code = 'starting'
            while code != 'exit' and code != "dead":
                if len(client.characters) < len(self.playersIP) + 2:
                    for address in self.playersIP:
                        start_game_packet = IP(dst=address, src=self.local_ip) / UDP(dport=self.dst_port,
                                                                                     sport=self.dst_port) / Raw(
                            load="starting")
                        scapy.sendrecv.send(start_game_packet)
                code = client.run()
        else:
            client = Client(self.IPJoined)
            code = 'starting'
            while code != 'exit' and code != "dead":
                code = client.run()

        return code


if __name__ == "__main__":
    useLocal = None
    code = "aight"
    while code != "exit":
        menu = Menu(useLocalGiven=useLocal)
        code = menu.run()
        useLocal = menu.useLocal
