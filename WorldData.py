import random

import pygame

from physicsObject import *
from staticGameObject import *
from CharacterBody import CharacterBody
import time

from Server import *

from Bomb import *

from RenderData import *

from Lazer import *

from enemy_bat import *
from waters.DamageOrb import DamageOrb
from waters.HealOrb import HealOrb
from waters.bat_snake import EnemySnakeBat

Black = (0, 0, 0)
White = (255, 255, 255)
Red = (255, 10, 10)
Blue = (10, 10, 255)

class WorldData:
    def __init__(self, IP):
        self.heart = CharacterBody(Vector2D(500, 200), 20, Red)

        self.worldSeed = random.randint(0, 1000)
        print("seedAAAAAAAA", self.worldSeed)
        # self.staticTerrain = StaticGameObjectGroup.makeNoiseGeneratedTerrain(0, 0, 1280, 700, 2, 9, self.worldSeed, 0.2,
        #                                                                      10, Black, self.heart.physics.radius, 10)
        self.staticTerrain = StaticGameObjectGroup.makeNoiseGeneratedTerrain(-1280*2, -700*2, 1280*5, 700*5, 2, 5, self.worldSeed, 0.2,
                                                                             10, Black, self.heart.physics.radius, 10)
        self.hands = []
        self.timePassed = time.time()

        self.server = Server(IP)
        self.server.seed = self.worldSeed

        self.render_data = RenderData()

        self.projectiles = []
        self.enemies = []
        self.items = []

        self.wave_number = 0

        self.current_wave_time = 0
    def update(self):
        self.hands = []
        colliders = []
        for hand in list(self.server.players.values()):
            self.hands.append(hand.character)
            colliders.append(self.hands[-1])
        chars = [self.heart]
        chars.extend([charac.character for charac in list(self.server.players.values())])
        self.render_data.characters = chars
        self.render_data.projectiles = self.projectiles
        self.render_data.wave_number = self.wave_number
        self.render_data.items = self.items
        responded_clients = self.server.accept_and_update(self.render_data)

        colliders.append(self.heart)
        for b in self.enemies:
            colliders.append(b)

        EnemyBat.checkLiveCollisions(colliders)

        newTime = time.time()
        dt = newTime - self.timePassed
        self.timePassed = newTime


        self.run_waves(dt)



        hitables = [self.heart]

        for i, enemy in enumerate(self.enemies):
            enemy.updatePhysics(dt, self.staticTerrain, self.heart)

            if enemy.hp <= 0:
                if not enemy.stuckDead:
                    if random.random() > 0.5:
                        self.items.append(HealOrb(enemy.physics.pos))
                    else:
                        self.items.append(DamageOrb(enemy.physics.pos))
                self.enemies.pop(i)
            else:
                hitables.append(enemy)

        for i, projectile in enumerate(self.projectiles):
            projectile.updatePhysics(dt, self.staticTerrain, self.enemies)
            if projectile.explode:
                projectile.on_death(hitables)
                self.projectiles.pop(i)

        for i, item in enumerate(self.items):
            if item.update(dt):
                self.items.pop(i)
            else:
                if (item.pos - self.heart.physics.pos).magnitude() < item.radius + self.heart.physics.radius:
                    item.onPickUp(self.heart)
        self.heart.physics.applyGravity()

        for player_socket in list(self.server.players.keys()):

            player = self.server.players[player_socket]
            player_left = False

            player.character.pullToMouse(player.mousePos, self.heart, 100)
            player.character.physics.applyForces(dt)
            if player.character.isHolding:
                player.character.color = player.character.heldColor
                player.character.physics.radius = player.character.heldRadius
            else:
                player.character.color = player.character.originalColor
                player.character.physics.radius = player.character.normalRadius

            if player.updated:
                player.updated = False
                for event in player.events:
                    event_dict = event[0]
                    event_type = event[1]
                    print("event_typeeeeeeeeeeeee", event_type)
                    if event_type == pygame.MOUSEBUTTONDOWN:
                        player.held["mouse 1"] = True
                        player.character.isHolding = player.held["mouse 1"]
                    if event_type == pygame.MOUSEBUTTONUP:
                        player.held["mouse 1"] = False
                        player.character.isHolding = player.held["mouse 1"]

                    if event_type == pygame.QUIT:
                        player_left = True
                        print("player lefttttttttTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT")

                    if player.character.shooting_cooldown <= 0:



                        if 'unicode' in event_dict:
                            if event_dict['unicode'] == 'e':
                                player.character.shooting_cooldown = 0.1
                                lazer = Lazer(player.character.physics.pos, 4, 15 * self.heart.damageMultiplyer, 100, 3)
                                lazer.physics.vel = (
                                                               player.character.physics.pos - self.heart.physics.pos).norm() * 1000 + player.character.physics.vel + self.heart.physics.vel
                                self.projectiles.append(lazer)
                            if event_dict['unicode'] == 'q':
                                player.character.shooting_cooldown = 0.5
                                print("bomb")
                                bomb = Bomb(player.character.physics.pos, 10, 25 * self.heart.damageMultiplyer, 100, 1)
                                bomb.physics.vel = (
                                                               player.character.physics.pos - self.heart.physics.pos).norm() * 100 + player.character.physics.vel + self.heart.physics.vel
                                self.projectiles.append(bomb)
            if player_left:
                print("before removing")
                self.server.players.pop(player_socket)
                self.server.all_sockets.remove(player_socket)
                print("removed")

        print("finished all players")



        self.heart.physics.applyForces(dt)

        for player in self.server.players.values():
            hand = player.character
            hand.shooting_cooldown -= dt
            if hand.stuckSpawn < 1:
                counterVel, collided = hand.physics.collideWithStaticGroup(self.staticTerrain)
            else:
                collided = hand.physics.isTouchingTerrain(self.staticTerrain)
                if not collided:
                    hand.stuckSpawn -= 1
                else:
                    hand.stuckSpawn = 5
                counterVel = Vector2D(0, 0)

            self.heart.pullToHandCounter(hand, counterVel, collided, power=20, mouseMovement=player.getMouseMovement())


            if not hand.isHolding:
                if counterVel.magnitude() <= 0.001:
                    hand.physics.vel += self.heart.physics.vel
                    hand.physics.updatePos(dt)
                    hand.physics.vel -= self.heart.physics.vel
                else:
                    hand.physics.updatePos(dt)

        if self.heart.stuckSpawn<1:
            self.heart.physics.collideWithStaticGroup(self.staticTerrain)
        else:
            collided = self.heart.physics.isTouchingTerrain(self.staticTerrain)
            if not collided:
                self.heart.stuckSpawn -= 1
            else:
                self.heart.stuckSpawn = 5


        self.heart.physics.updatePos(dt)
        self.heart.invincibleTime -= dt


        return responded_clients

    def isDead(self):
        return self.heart.hp <= 0

    def send_data(self, responded_clients):
        chars = [self.heart]
        chars.extend([charac.character for charac in list(self.server.players.values())])
        self.render_data.characters = chars
        self.render_data.projectiles = self.projectiles
        self.render_data.enemies = self.enemies
        self.render_data.wave_number = self.wave_number
        self.render_data.items = self.items
        self.server.sendRenderData(self.render_data, responded_clients)

    def run_waves(self, dt):
        new_wave = False

        if len(self.server.players) > 0 and self.wave_number == 0:
            self.wave_number = 1

        if self.wave_number != 0:
            self.current_wave_time += dt
            if self.current_wave_time > 10:
                self.wave_number += 1
                new_wave = True
                self.current_wave_time = 0

        enemy_num = self.wave_number * 2

        if self.wave_number < 1:
            if enemy_num - len(self.enemies) > 0:
                for _ in range(enemy_num - len(self.enemies)):
                    pos = self.heart.physics.pos + Vector2D(random.randint(1000, 1200), 0).rotate(random.randint(0, 7))
                    self.enemies.append(
                        EnemyBat(
                            pos, min(10 + self.wave_number, 15), 40 + self.wave_number*2, 10+self.wave_number, self.wave_number//5 + 1
                        )
                    )

        else:
            if new_wave:
                pos = self.heart.physics.pos + Vector2D(random.randint(1000, 1200), 0).rotate(random.randint(0, 7))
                head = EnemyBat(
                        pos, min(10 + self.wave_number, 15), 40 + self.wave_number * 2, 10 + self.wave_number,
                                                             self.wave_number // 5 + 1
                    )
                self.enemies.append(
                    head
                )
                first = head
                for i in range(5):
                    pos += Vector2D(min(10 + self.wave_number, 15) * 2, 0)
                    first = EnemySnakeBat(
                        pos, min(10 + self.wave_number, 15), 40 + self.wave_number * 2, 10 + self.wave_number,
                                                             self.wave_number // 5 + 1, first
                    )
                    self.enemies.append(first)

    def run_game(self):
        dead = False
        done = False
        while not done:
            if self.isDead():
                if not dead:
                    dead = True
                    sentDeathToAll = {sock: False for sock in list(self.server.players.keys())}
                print("deadddddddd")
                for sock in responded_clients:
                    sentDeathToAll[sock] = True
                print(sentDeathToAll)
            responded_clients = self.update()
            self.send_data(responded_clients)
            if dead:
                done = True
                for sent in list(sentDeathToAll.values()):
                    done = done and sent

        print("finished a game")


#
#
# if __name__ == "main":
#     world = WorldData()
#     print("made world")
#     world.run_game()


