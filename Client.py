import pickle
import socket
import threading
from Vector import  *

import pygame

from Protocol import *
from waters.staticGameObject import StaticGameObjectGroup

Black = (0, 0, 0)
White = (255, 255, 255)


class Client:

    def __init__(self, IP):
        self.my_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.my_socket.connect((IP, Protocol.PORT))

        print("socket made")

        self.worldSeed = self.retrieveWorldSeed()
        print("seed, ", self.worldSeed)

        render_data = self.retrieveRenderData()
        print(render_data)
        self.characters = render_data.characters
        self.projectiles = render_data.projectiles
        self.enemies = render_data.enemies
        self.character_index = len(self.characters)
        self.wave_number = render_data.wave_number
        self.items = render_data.items
        print("index", self.character_index)


        self.staticTerrain = StaticGameObjectGroup.makeNoiseGeneratedTerrain(-1280*2, -700*2, 1280*5, 700*5, 2, 5, self.worldSeed, 0.2, 10, Black, self.characters[0].physics.radius, 10)
        pygame.init()

        self.surface = pygame.display.set_mode((1280, 700), pygame.SRCALPHA)
        self.clock = pygame.time.Clock()

        self.font = pygame.font.Font(None, 20)
        self.text_surface = self.font.render("Wave Number: " + str(self.wave_number), True, Black)
        self.text_rect = self.text_surface.get_rect(center=(1280/2, 100))



    def retrieveRenderData(self):
        render_data = Protocol.recieveMessage(self.my_socket)
        render_data = pickle.loads(render_data)
        return render_data

    def retrieveWorldSeed(self):
        seed = Protocol.recieveMessage(self.my_socket).decode()
        try:
            seed = int(seed)
        except TypeError:
            print("seed not number?")
            return 0
        return seed

    def renderScreen(self, surface, offset):
        self.staticTerrain.render(surface, offset)
        self.characters[0].renderBodyWithHands(surface, self.characters[1:], offset)

        for projectile in self.projectiles:
            projectile.render(surface, offset)

        for enemy in self.enemies:
            enemy.render(surface, offset, self.characters[0])

        for item in self.items:
            item.render(surface, offset)

        surface.blit(self.text_surface, self.text_rect)


    def run(self):
        exit_code = "alright"

        events = pygame.event.get()

        eventsLst = []
        for event in events:
            if event.type == pygame.QUIT:
                exit_code = 'exit'
            eventsLst.append([event.dict, event.type])
            print("event_type", event.type)
        eventBytes = pickle.dumps(eventsLst)


        mx = str(pygame.mouse.get_pos()[0]).encode()# + self.characters[0].physics.pos.x - 400).encode() adding this in
        my = str(pygame.mouse.get_pos()[1]).encode()# + self.characters[0].physics.pos.y - 400).encode() server to also
                                                                                                        # get actual
        Protocol.sendMessage(self.my_socket, b"events:::"+eventBytes+b";;;"+b"mousePos:::"+mx+b","+my)  #mouse in server

        self.surface.fill(White)
        self.renderScreen(self.surface, self.characters[0].physics.pos - Vector2D(400, 400))

        pygame.display.update()

        render_data = self.retrieveRenderData()
        self.characters = render_data.characters
        self.projectiles = render_data.projectiles
        self.items = render_data.items
        self.enemies = render_data.enemies
        if render_data.wave_number != self.wave_number:
            self.wave_number = render_data.wave_number
            self.text_surface = self.font.render("Wave Number: " + str(self.wave_number), True, Black)
            self.text_rect = self.text_surface.get_rect(center=(1280 / 2, 100))

        if self.characters[0].hp <= 0:
            if exit_code != "exit":
                exit_code = "dead"

        self.clock.tick(40)
        return exit_code

# if __name__ == "main":
#     client = Client()
#     code = 'starting'
#     while code != 'exit':
#         code = client.run()



