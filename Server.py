import random
import socket
import select
import pickle

from Player import *

from waters.Protocol import Protocol
from CharacterHand import *

PORT = 80321


class Server:

    def __init__(self, IP):

        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # self.socketAddress = socket.gethostbyname(socket.gethostname())
        self.server_socket.bind((IP, Protocol.PORT))
        self.server_socket.listen()
        print("Server is up and running")
        self.players = {}
        self.seed = 0
        self.all_sockets = [self.server_socket]

    def accept_and_update(self, render_data):
        readers, writers, excepters = select.select(self.all_sockets, [], [], 0.01)

        responded_clients = []
        for read in readers:
            if read is self.server_socket:
                print("new client joined")
                client_socket, addr = self.server_socket.accept()
                self.all_sockets.append(client_socket)
                character = self.spawnCharacter()
                self.sendWorldSeed(self.seed, client_socket)
                self.sendRenderDataSingle(render_data, client_socket)
                print("seed,", self.seed)
                self.players.update({client_socket: Player(character, len(self.players))})
            else:
                message = Protocol.recieveMessage(player_socket=read)
                self.players[read].updatePlayerData(message)
                responded_clients.append(read)
        return responded_clients


    def spawnCharacter(self):
        character = CharacterHand(Vector2D(200, 200), 10, (random.randint(20, 240), random.randint(20, 240), random.randint(20, 240)))
        print(len(self.players),"th player spawned")
        return character

    def sendRenderData(self, render_data, responded_clients):
        if len(responded_clients) == 0:
            return
        data = pickle.dumps(render_data)
        for player_socket in responded_clients:
            Protocol.sendMessage(player_socket, data)

    def sendRenderDataSingle(self, render_data, socket):
        data = pickle.dumps(render_data)

        Protocol.sendMessage(socket, data)

    def sendWorldSeed(self, seed, socket):
        Protocol.sendMessage(socket, str(seed).encode())




