import socket
class Protocol:
    PORT = 32451
    FIELD_SIZE = 4

    @staticmethod
    def recieveMessage(player_socket):
        p = Protocol.recieveX(player_socket, Protocol.FIELD_SIZE)
        length_length = p.decode()

        try:
            length_length = int(length_length)
        except TypeError:
            print("WARNING: length_length not an int")
            return ""

        length = Protocol.recieveX(player_socket, length_length).decode()

        try:
            length = int(length)
        except TypeError:
            print("WARNING: length not an int")
            return ""

        message = Protocol.recieveX(player_socket, length)
        return message






    @staticmethod
    def recieveX(input_socket: socket, x):
        message = b""
        while len(message) < x:
            message += input_socket.recv(x-len(message))
        if len(message) != x:
            print("WARNING: didn't receive x amount")
        return message

    @staticmethod
    def sendMessage(socket, message):
        length = len(message)
        length_length = len(str(length))
        full_message = str(length_length).zfill(Protocol.FIELD_SIZE).encode() + str(length).encode() + message
        socket.send(full_message)

